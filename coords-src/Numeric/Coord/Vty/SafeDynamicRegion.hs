{-# LANGUAGE TemplateHaskell #-}

module Numeric.Coord.Vty.SafeDynamicRegion
  ( SafeDynamicRegion (..)
  , safeDynamicRegionTopLeft
  , safeDynamicRegionSize
  ) where

-- Internal imports

import Numeric.Coord

-- External imports

import Control.Lens
  ( makeLenses
  )

import Reflex.Vty
  ( Dynamic
  )

data SafeDynamicRegion t = SafeDynamicRegion
  { _safeDynamicRegionTopLeft :: Dynamic t (Coord Int Int)
  , _safeDynamicRegionSize    :: Dynamic t (Coord Int Int)
  }
makeLenses ''SafeDynamicRegion
