module Numeric.Coord.Linear
  ( linearApplication
  , dot
  ) where

-- Interal imports

import Numeric.Coord.Internal

-- | Applies a linear function to a coordinate
-- Takes the linear function as a two by two matrix.
linearApplication ::
  ( Num x
  )
    => (x, x, x, x) -> Coord x x -> Coord x x
linearApplication (m00, m01, m10, m11) (X x, Y y) =
  ( X $ m00 * x + m01 * y
  , Y $ m10 * x + m11 * y
  )

-- | Performs the dot product
dot :: (Num a) => Coord a a -> Coord a a -> a
dot (X x1, Y y1) (X x2, Y y2) = x1 * x2 + y1 * y2
