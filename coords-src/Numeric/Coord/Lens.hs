module Numeric.Coord.Lens
  ( xAndy
  ) where

-- Internal imports

import Numeric.Coord

-- External imports

import Control.Lens

-- | A 'Setter' that operates on both the 'X' and 'Y' coordinates of a 'Coord'.
-- For example, the following code will scale both x and y coordinates by 2:
--
-- > over xAndy (*2)
xAndy :: Setter (Coord a a) (Coord b b) a b
xAndy = sets (\ f -> over (_2 . mapped) f . over (_1 . mapped) f)
