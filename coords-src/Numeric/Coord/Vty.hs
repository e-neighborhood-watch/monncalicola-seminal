module Numeric.Coord.Vty
  ( coordDisplaySize
  , coordPad
  , coordPane
  , module Numeric.Coord.Vty.SafeDynamicRegion
  ) where

-- Internal imports

import Numeric.Coord.Internal
import Numeric.Coord.Vty.SafeDynamicRegion

-- External imports

import Control.Applicative
import Control.Lens

import Graphics.Vty
  ( Image
  , pad
  )

import Reflex.Vty
  ( Dynamic
  , DynRegion (DynRegion)
  , HasDisplaySize
  , MonadNodeId
  , Reflex
  , VtyWidget
  , _dynRegion_height
  , _dynRegion_left
  , _dynRegion_top
  , _dynRegion_width
  , displayHeight
  , displayWidth
  , pane
  , splitDynPure
  )

-- | A version of 'pad' from Graphics.Vty which works on coordinates
coordPad ::
  ( Integral a
  , Integral b
  , Integral c
  , Integral d
  ) 
    => Coord a b   -- ^ Padding on the left and top sides of the image
      -> Coord c d -- ^ Padding on the right and bottom sides of the image
        -> Image -> Image
coordPad (X a, Y b) (X c, Y d) =
  pad (fromIntegral a) (fromIntegral b) (fromIntegral c) (fromIntegral d)

-- | A version of 'pane' from Relfex.Vty which works on coordinates by means of the 'SafeDynamicRegion' type.
coordPane ::
  ( Reflex t
  , MonadNodeId m
  )
    => SafeDynamicRegion t
      -> Dynamic t Bool
        -> VtyWidget t m a
          -> VtyWidget t m a
coordPane paneRegion = pane unsafeDynamicRegion
  where
    (left, top) = splitDynPure $ view safeDynamicRegionTopLeft paneRegion
    (width, height) = splitDynPure $ view safeDynamicRegionSize paneRegion
    unsafeDynamicRegion = DynRegion
      { _dynRegion_left   = view unX <$> left
      , _dynRegion_top    = view unY <$> top
      , _dynRegion_width  = view unX <$> width
      , _dynRegion_height = view unY <$> height
      }

coordDisplaySize ::
  ( HasDisplaySize t m
  )
    => m (Dynamic t (Coord Int Int))
coordDisplaySize = do
  width <- displayWidth
  height <- displayHeight
  return $ liftA2 (,) (X <$> width) (Y <$> height)
