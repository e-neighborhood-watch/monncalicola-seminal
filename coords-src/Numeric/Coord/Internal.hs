{-# Language GeneralizedNewtypeDeriving #-}
{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
{-# Language DeriveFunctor #-}
module Numeric.Coord.Internal
  ( Coord
  , X (..)
  , Y (..)
  , unX
  , unY
  )
  where

-- External imports

import Control.Lens

import Data.Hashable

-- | A horizontal coordinate.
-- Prevents accidental use of horizontal coordinate as vertical.
newtype X a = X
  { _unX :: a
  }
  deriving
    ( Show
    , Eq
    , Ord
    , Num
    , Functor
    , Hashable
    , Enum
    )
makeLenses ''X

-- | A vertical coordinate.
-- Prevents accidental use of vertical coordinate as horizontal.
newtype Y a = Y
  { _unY :: a
  }
  deriving
    ( Show
    , Eq
    , Ord
    , Num
    , Functor
    , Hashable
    , Enum
    )
makeLenses ''Y

type Coord a b = (X a, Y b)

instance (Num a, Num b) => Num (Coord a b) where
  (a, b) + (c, d) = (a + c, b + d)
  (a, b) * (c, d) = (a * c, b * d)
  abs (a, b) = (abs a, abs b)
  signum (a, b) = (signum a, signum b)
  fromInteger x = (fromInteger x, fromInteger x)
  negate (a, b) = (negate a, negate b)
