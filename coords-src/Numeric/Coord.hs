{-# Options_GHC -fno-warn-redundant-constraints #-}
module Numeric.Coord
  ( Coord
  , X ()
  , Y ()
  , mkX
  , mkY
  ) where

-- Internal imports

import Numeric.Coord.Internal

-- | Exported constructor for X
-- ensures the contents are a Num
mkX ::
  ( Num a
  )
    => a -> X a
mkX a = X a

-- | Exported constructor for Y
-- ensures the contents are a Num
mkY ::
  ( Num a
  )
    => a -> Y a
mkY a = Y a

