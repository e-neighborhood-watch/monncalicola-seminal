{-# Language Rank2Types #-}
{-# Language KindSignatures #-}
module Game.Lens
  ( andAlso
  )
  where

-- External imports

import Control.Lens
import Data.Kind
  ( Type
  )

andAlso ::
  forall (f :: Type -> Type) s1 s2 a b t.
    Settable f =>
      ASetter s1 s2 a b
        -> ASetter s2 t a b
          -> Optical (->) (->) f s1 t a b
andAlso s1 s2 = sets (\ f -> over s2 f . over s1 f)

