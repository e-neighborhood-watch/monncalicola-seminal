{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}

module Game.Main
 ( main
 ) where

-- Internal imports

import Game.Widget.Game

-- External imports

import Reflex.Vty
  ( mainWidget
  )

main :: IO ()
main = mainWidget $ gameWidget
