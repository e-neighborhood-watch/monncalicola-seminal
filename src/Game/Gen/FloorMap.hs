{-# LANGUAGE ScopedTypeVariables #-}

module Game.Gen.FloorMap where

-- Internal imports

import Game.Gen.Simplex

import Numeric.Coord

-- External imports

import Control.Lens
import Control.Monad.Reader

class HasTerrainSeed r where
  terrainSeed :: Lens' r Int

instance HasTerrainSeed Int where
  terrainSeed = id

isWall ::
  ( MonadReader r m
  , HasGameSeed r
  , HasTerrainSeed r
  )
    => (X Integer, Y Integer, Integer)
      -> m Bool
isWall loc = do
  terrainS <- view terrainSeed <$> ask
  (res :: Double) <- simplex terrainS 12 loc
  return $ res <= 0.1
