{-# LANGUAGE ScopedTypeVariables #-}

module Game.Gen.Simplex where

-- Internal imports

import Numeric.Coord
import Numeric.Coord.Lens
import Numeric.Coord.Linear

-- External imports

import Control.Lens
import Control.Monad.Reader
import Data.Hashable
import System.Random

class HasGameSeed r where
  gameSeed :: Lens' r Int

instance HasGameSeed Int where
  gameSeed = id

pseudorandom :: Int -> Int
pseudorandom = fst . random . mkStdGen

-- | Generates the value of the simplex noise map at a given point with a given
-- game seed, position, scale, and an additional seed for the specific task the
-- result will be used for.
simplex ::
  forall float r m.
  ( MonadReader r m
  , HasGameSeed r
  , Floating float
  , RealFrac float
  )
    => Int                                 -- ^ The task specific seed
      -> float                             -- ^ The side length of the simplices
        -> (X Integer, Y Integer, Integer) -- ^ The position in the form of (x, y, depth)
          -> m float                       -- ^ The value of the simplex noise map for a game seed (ranges from -1 to 1)
simplex taskSeed scale (posX, posY, depth) = do
  gameS <- view gameSeed <$> ask
  let
    seed :: Int
    seed = taskSeed + gameS

    scaled :: Coord float float
    scaled = over xAndy ((/ scale) . fromIntegral) (posX, posY)

    skewFactor :: float
    skewFactor = sqrt 3 / 2

    skewed :: Coord float float 
    skewed = linearApplication
      ( skewFactor + 1/2, skewFactor - 1/2
      , skewFactor - 1/2, skewFactor + 1/2
      )
      scaled

    base :: Coord Integer Integer
    base = over xAndy floor skewed

    rel :: Coord float float
    rel = skewed - over xAndy fromInteger base

    point1 :: Coord Integer Integer
    point1 =
      if view _1 (linearApplication (1, -1, 1, 1) rel) > 0
        then (1, 0) + base
        else (0, 1) + base

    point2 :: Coord Integer Integer
    point2 = (1, 1) + base

    gradient0 :: Coord float float
    gradient0 = selectGradient $ pseudorandom $ hashWithSalt seed (view _1 base, view _2 base, depth)

    gradient1 :: Coord float float
    gradient1 = selectGradient $ pseudorandom $ hashWithSalt seed (view _1 point1, view _2 point1, depth)

    gradient2 :: Coord float float
    gradient2 = selectGradient $ pseudorandom $ hashWithSalt seed (view _1 point2, view _2 point2, depth)

    unskew :: Coord Integer Integer -> Coord float float
    unskew s = linearApplication
      ( 1/4 * (1/skewFactor + 2), 1/4 * (1/skewFactor - 2) 
      , 1/4 * (1/skewFactor - 2), 1/4 * (1/skewFactor + 2) 
      )
      (over xAndy fromInteger s)

    contribution :: Coord Integer Integer -> Coord float float -> float
    contribution point gradient = dot offset gradient * max 0 (0.5 - squaredDist) ^ (4 :: Integer)
      where
        offset :: Coord float float
        offset = scaled - unskew point

        squaredDist :: float
        squaredDist = dot offset offset

  return
    $ 70 * sum
      [ contribution base   gradient0
      , contribution point1 gradient1
      , contribution point2 gradient2
      ]

selectGradient ::
  ( Floating a
  )
    => Int         -- ^ The index of the gradient
      -> Coord a a -- ^ The gradient represented as a 2D vector
selectGradient ind =
  [ ( mkX ( 1  ) , mkY ( 0  ) )
  , ( mkX ( 0.8) , mkY ( 0.6) )
  , ( mkX ( 0.6) , mkY ( 0.8) )
  , ( mkX ( 0  ) , mkY ( 1  ) )
  , ( mkX (-0.6) , mkY ( 0.8) )
  , ( mkX (-0.8) , mkY ( 0.6) )
  , ( mkX (-1  ) , mkY ( 0  ) )
  , ( mkX (-0.8) , mkY (-0.6) )
  , ( mkX (-0.6) , mkY (-0.8) )
  , ( mkX ( 0  ) , mkY (-1  ) )
  , ( mkX ( 0.6) , mkY (-0.8) )
  , ( mkX ( 0.8) , mkY (-0.6) )
  ] !! (ind `mod` 12)
