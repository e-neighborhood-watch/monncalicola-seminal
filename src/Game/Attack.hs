{-# Language FlexibleContexts #-}
module Game.Attack where

import Game.Type.Attack
import Game.Type.Body
import Game.Type.Log
import Game.Type.Name

import Control.Lens
import Control.Monad.Writer

-- | Ensures a body part is destroyed iff the damage exceeds the maximum
destroyPart ::
  BodyPart -> BodyPart
destroyPart part =
  set intact (0 < view bloodML part) part

-- | Performs an attack targeting a place on a thing with body.
-- Since both players and enemies have bodies this can be used on both.
performAttack ::
  ( HasBody b
  , MonadWriter [ Log ] m
  ) => Attack   -- ^ The attack that is being performed
    -> Name     -- ^ The name of the body part being targetted
      -> b      -- ^ Something with a body
        -> m b  -- ^ The new body is yeilded from a monad that creates the logs
performAttack attack targetName b = do
  tell [ "Attack performed" ]  -- TODO
  return $
    over
      (body . at targetName . mapped)
      (destroyPart . over bloodML (subtract $ view damageAmount attack))
      b
