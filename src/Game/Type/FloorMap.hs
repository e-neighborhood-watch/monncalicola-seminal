{-# Language TemplateHaskell #-}
module Game.Type.FloorMap where

-- Internal imports

import Game.Type.Player
import Game.Type.Enemy

import Numeric.Coord

-- External imports

import Control.Lens
import Data.Map hiding
  ( size
  )

data FloorMap = FloorMap
  { _playerLocation :: Coord Integer Integer
  , _player         :: Player
  , _size           :: Coord Integer Integer
  , _entities       :: Map (Coord Integer Integer) Enemy
  }
makeLenses ''FloorMap

playerX :: Lens' FloorMap (X Integer)
playerX = playerLocation . _1

playerY :: Lens' FloorMap (Y Integer)
playerY = playerLocation . _2

sizeW :: Lens' FloorMap (X Integer)
sizeW = size . _1

sizeH :: Lens' FloorMap (Y Integer)
sizeH = size . _2

