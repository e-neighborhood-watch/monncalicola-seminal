{-# LANGUAGE TemplateHaskell #-}

module Game.Type.Attack where

import qualified Game.Type.Name as N

import Control.Lens

-- | An attack initiated by a creature (e.g. 'Enemy' or 'Player').
-- An attack on its own has not target.
data Attack = Attack
  { _name         :: N.Name  -- ^ The name for the attack, it will be refered to by this.
  , _damageAmount :: Integer -- ^ The amount of blood in milliliters removed from the target.
                             -- This will be removed when the blood system is revamped.
  }
makeLenses ''Attack

instance N.HasName Attack where
  name = name
