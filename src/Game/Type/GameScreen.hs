module Game.Type.GameScreen
  ( GameScreen (..)
  ) where

data GameScreen
  = MainMenu
  | WalkMap
--  | AttackMenu
  deriving (Eq, Show)
