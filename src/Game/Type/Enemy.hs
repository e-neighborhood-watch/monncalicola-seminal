{-# LANGUAGE TemplateHaskell #-}

module Game.Type.Enemy
  ( Enemy (Enemy)
  ) where

-- Internal imports

import qualified Game.Type.Body as B
import qualified Game.Type.Name as N

-- External imports

import Control.Lens

-- | A creature that the player can attack.
data Enemy = Enemy
  { _name :: N.Name  -- ^ What the enemy will be refered to as.
  , _icon :: Char    -- ^ How the enemy is displayed on the map.
  , _body :: B.Body
  }
makeLenses ''Enemy

instance N.HasName Enemy where
  name = name

instance N.HasIcon Enemy where
  icon = icon

instance B.HasBody Enemy where
  body = body
