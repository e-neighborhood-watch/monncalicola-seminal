{-# LANGUAGE TemplateHaskell #-}

module Game.Type.Player
  ( Player (Player)
  ) where

import qualified Game.Type.Body as B
import qualified Game.Type.Name as N

import Control.Lens

-- | Represents the avatar for the user
data Player = Player
  { _name :: N.Name  -- ^ What the player will be refered to as
  , _icon :: Char    -- ^ How the player will be represented on the map
  , _body :: B.Body  -- ^ The players corporeal body
  }
makeLenses ''Player

instance N.HasName Player where
  name = name

instance N.HasIcon Player where
  icon = icon

instance B.HasBody Player where
  body = body
