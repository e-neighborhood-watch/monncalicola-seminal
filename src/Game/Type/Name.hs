module Game.Type.Name where

import Control.Lens

type Name = String

class HasName a where
  name :: Lens' a Name

class HasIcon a where
  icon :: Lens' a Char
