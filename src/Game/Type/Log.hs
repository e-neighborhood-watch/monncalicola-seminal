module Game.Type.Log
  ( Log
  ) where

-- | Logs describing events in the game.
-- These will be displayed to the player.
type Log = String

