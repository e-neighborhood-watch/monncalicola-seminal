{-# Language TemplateHaskell #-}
{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}

module Game.Type.Body where

import Game.Type.Name

import Control.Lens
import Data.Map

-- | A body is a collection of body parts accessed by name.
-- The map provides a guarentee that names are unique.
type Body = Map Name BodyPart

-- | A body part represents the fundamental unit of a body.
data BodyPart = BodyPart
  { _grasper :: Bool     -- ^ Whether this part can hold a tool.
  , _vital   :: Bool     -- ^ Whether destroying the part kills the owner.
  , _support :: Bool     -- ^ Whether destroying the part makes the owner prone.
  , _intact  :: Bool     -- ^ Whether the part is currently intact.
  , _bloodML :: Integer  -- ^ Blood in the part in milliliters.
                         -- Acts as hit points, when they hit zero the part is no longer intact.
                         -- To be removed in the future for a less stupid system.
  }
  deriving
    ( Show
    )
makeLenses ''BodyPart

-- | Things in this class have a body.
-- And as such they can be attacked and killed, use tools etc.
class HasBody b where
  -- | A lens that gives us access to they body of `b`.
  body :: Lens' b Body

instance HasBody Body where
  body = id
