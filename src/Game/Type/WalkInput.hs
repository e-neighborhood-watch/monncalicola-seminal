module Game.Type.WalkInput where

-- External imports

import Graphics.Vty
import Reflex.Vty
  ( VtyEvent
  )

-- | Represents player interaction with the game after being processed.
data WalkInput
  = WalkLeft
  | WalkRight
  | WalkUp
  | WalkDown
  | Quit
  deriving
    ( Eq
    )

-- | Converts raw keyboard input into actions which can be easily manipulated.
-- Output is a `Maybe` with `Nothing` meaning no action corresponds to the given `VtyEvent`.
filterInput ::
  VtyEvent             -- ^ Raw keyboard input.
    -> Maybe WalkInput -- ^ Optional processed output.
filterInput (EvKey KLeft []) = Just WalkLeft
filterInput (EvKey KRight []) = Just WalkRight
filterInput (EvKey KUp []) = Just WalkUp
filterInput (EvKey KDown []) = Just WalkDown
filterInput (EvKey (KChar 'q') []) = Just Quit
filterInput _ = Nothing
