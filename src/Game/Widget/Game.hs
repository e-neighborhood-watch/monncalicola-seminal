{-# LANGUAGE RecursiveDo #-}

module Game.Widget.Game
  ( gameWidget
  ) where

-- Internal imports

import Game.Type.GameScreen

import Game.Widget.MainMenu
import Game.Widget.WalkMap

-- External imports

import Control.Monad.Fix

import Reflex.Network
import Reflex.Vty

-- | Associates the 'GameScreen' enum type to the associated `VtyWidget` object
selectGameSubWidget ::
  ( PostBuild t m
  , MonadFix m
  , MonadHold t m
  , MonadNodeId m
  )
    => GameScreen
      -> VtyWidget t m (Event t (Either () GameScreen))
selectGameSubWidget MainMenu = mainMenuWidget
selectGameSubWidget WalkMap = walkMapWidget
--selectGameSubWidget AttackMenu = attackMenuWidget

-- | The highest level of widget. Handles switching between various screens in the game.
gameWidget ::
  ( PostBuild t m
  , Adjustable t m
  , MonadFix m
  , MonadHold t m
  , MonadNodeId m
  )
    => VtyWidget t m (Event t ())
gameWidget = do
  rec
    (quitE, switchGameScreenE)
      <- fmap (fanEither . switch . current)
        $ networkHold mainMenuWidget
          $ fmap selectGameSubWidget
            $ switchGameScreenE
  return $ quitE
