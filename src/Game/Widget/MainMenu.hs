{-# LANGUAGE OverloadedStrings #-}

module Game.Widget.MainMenu
  ( mainMenuWidget
  ) where

-- Internal imports

import Game.Type.GameScreen

-- External imports

import Control.Monad.Fix

import Reflex.Vty

mainMenuWidget ::
  ( PostBuild t m
  , MonadFix m
  , MonadHold t m
  , MonadNodeId m
  )
    => VtyWidget t m (Event t (Either () GameScreen))
mainMenuWidget = do
  col $ do
    walkMapE <- fixed 5 $ textButtonStatic def "Start Game"
    quitE <- fixed 5 $ textButtonStatic def "Quit"
    return $ leftmost [ Left () <$ quitE , Right WalkMap <$ walkMapE ]
