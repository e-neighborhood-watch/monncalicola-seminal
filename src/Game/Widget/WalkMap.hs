module Game.Widget.WalkMap
  ( walkMapWidget
  ) where

-- Internal imports

import Game.Type.Body
import Game.Type.Enemy
import Game.Type.FloorMap
import Game.Type.GameScreen
import Game.Type.Player
import Game.Type.WalkInput

import Game.Widget.Map

-- External imports

import Control.Lens
import Control.Monad.Fix

import Data.Map
  ( fromList
  )

import Graphics.Vty.Input hiding
  ( Event
  )

import Reflex.Vty

createMap :: FloorMap
createMap = FloorMap
  { _playerLocation = (0,0)
  , _player = Player
    "Player" -- To be changed
    '@' $
    fromList 
      [ ( "Left Arm"
        , BodyPart
          { _grasper = True
          , _vital   = False
          , _support = False
          , _intact  = True
          , _bloodML = 100
          }
        )
      , ( "Left Arm"
        , BodyPart
          { _grasper = True
          , _vital   = False
          , _support = False
          , _intact  = True
          , _bloodML = 100
          }
        )
      , ( "Left Arm"
        , BodyPart
          { _grasper = False
          , _vital   = False
          , _support = True
          , _intact  = True
          , _bloodML = 200
          }
        )
      , ( "Left Arm"
        , BodyPart
          { _grasper = False
          , _vital   = False
          , _support = True
          , _intact  = True
          , _bloodML = 200
          }
        )
      , ( "Left Arm"
        , BodyPart
          { _grasper = False
          , _vital   = True
          , _support = False
          , _intact  = True
          , _bloodML = 100
          }
        )
      , ( "Left Arm"
        , BodyPart
          { _grasper = False
          , _vital   = True
          , _support = False
          , _intact  = True
          , _bloodML = 50
          }
        )
      ]
  , _size = (63, 31)
  , _entities = fromList
    [ ( ( 1, 2)
      , Enemy
        "Ghoel"
        'Y' $
        fromList []
      )
    ]
  }

walkInput :: (Reflex t, Monad m, HasVtyInput t m) => m (Event t WalkInput)
walkInput = fmapMaybe filterInput <$> input

updateMap :: FloorMap -> WalkInput -> Maybe FloorMap
updateMap m walkEvent =
  do
    let
      tryUpdate :: Maybe FloorMap
      tryUpdate =
        case walkEvent of
          WalkLeft -> Just $ over playerX (subtract 1) m
          WalkRight -> Just $ over playerX (+ 1) m
          WalkUp -> Just $ over playerY (subtract 1) m
          WalkDown -> Just $ over playerY (+ 1) m
          _ -> Nothing
    updatedMap <- tryUpdate
    return updatedMap

walkMapWidget ::
  ( MonadHold t m
  , MonadFix m
  , Reflex t
  )
    => VtyWidget t m (Event t (Either () GameScreen))
walkMapWidget = do
  quitE <- key (KChar 'q')
  mainMenuE <- key (KChar 'm')
  inp <- walkInput
  floorMapB <- accumMaybeB updateMap createMap inp
  mapWidget floorMapB
  return $ leftmost [ Left () <$ quitE , Right MainMenu <$ mainMenuE ]

