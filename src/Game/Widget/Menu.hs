{-# LANGUAGE ScopedTypeVariables #-}

module Game.Widget.Menu
  ( menuSystemWidget
  )
  where

-- Internal imports

import Game.Widget.Switchable

-- External imports

import Control.Monad
import Control.Monad.Choice.Covariant
import Control.Monad.Fix

import Data.Functor.Compose
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NonEmpty
import Data.Text

import Graphics.Vty.Input hiding (Event)

import Reflex.Vty

type Container = Compose ((,) Text) (Compose NonEmpty ((,) Text))

-- | Builds a system of menus from a 'Choice' monad.
menuSystemWidget :: (PostBuild t m, MonadNodeId m, MonadHold t m, MonadFix m, Adjustable t m) => Choice Container a -> Switchable t (VtyWidget t m) (Maybe a)
menuSystemWidget = runBacktrackableChoiceM menuWidget

-- | Builds a single menu from a 'Container'. Note that
-- this function behaves the same regardless of the type
-- of the contents of the 'Container'.
menuWidget :: forall t m a. (Reflex t, MonadNodeId m, MonadHold t m, MonadFix m) => Container a -> Switchable t (VtyWidget t m) (Maybe a)
menuWidget (Compose (menuName, Compose menuOptions)) =
  Switchable $
    fmap snd $
      splitV (pure $ const 2) (pure (False, True)) (text $ pure menuName) $
        boxStatic def $
          do
            upE <- (subtract 1 <$) <$> key KUp
            downE <- ((+1) <$) <$> key KDown
            backE <- void <$> key KEsc
            selectE <- void <$> key KEnter
            let
              scrollingE :: Event t (Int -> Int)
              scrollingE = mergeWith (.) [upE, downE]
            menuIndex <- accumB (\n f -> min (NonEmpty.length menuOptions - 1) $ max 0 $ f n) 0 scrollingE
            let
              currentSelection :: Behavior t a
              currentSelection = snd . (NonEmpty.!!) menuOptions <$> menuIndex
            return $ leftmost [Nothing <$ backE, fmap Just currentSelection <@ selectE]
