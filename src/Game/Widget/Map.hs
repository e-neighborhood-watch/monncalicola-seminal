{-# Language ScopedTypeVariables #-}
{-# Language BangPatterns #-}
module Game.Widget.Map where

-- Internal imports

import Game.Gen.FloorMap
import Game.Type.Enemy
import Game.Type.FloorMap
import Game.Type.Name

import Numeric.Coord
import Numeric.Coord.Vty

-- External imports

import Prelude hiding
  ( lookup
  )
import Control.Lens
import Data.Map
  ( lookup
  )
import Graphics.Vty
  ( Image
  , defAttr
  , vertCat
  , string
  , char
  , emptyImage
  , (<|>)
  , backgroundFill
  , translateX
  )
import Reflex.Vty hiding
  ( text
  )

mapWidget :: (Monad m, Reflex t) => Behavior t FloorMap -> VtyWidget t m ()
mapWidget viewer = do
  tellImages $ mapImages <$> viewer

mapImages :: FloorMap -> [Image]
mapImages theMap =
  -- Draw the background
  [ vertCat
    [ string defAttr
      [ boolToTile $ isWall (offsetX + view playerX theMap, offsetY + view playerY theMap, 0) (0 :: Int)
      | offsetX <- [ -leftOffset .. rightOffset ]
      ]
    | offsetY <- [ -topOffset .. bottomOffset ]
    ]
  -- Draw the enemies
  , vertCat $
    map (drawRow findEnemy (-leftOffset) rightOffset) [ -topOffset .. bottomOffset ]
  -- Draw the player
  , coordPad (leftOffset, topOffset) (rightOffset, bottomOffset) $ char defAttr $ view (player . icon) theMap
  ]
  where
    leftOffset   = fmap (`div` 2) $ view sizeW theMap
    topOffset    = fmap (`div` 2) $ view sizeH theMap
    rightOffset  = view sizeW theMap - leftOffset - 1
    bottomOffset = view sizeH theMap - topOffset  - 1
    boolToTile bool = if bool then '#' else '.'
    findEnemy ::
      Coord Integer Integer -- ^ the displacement from the player
        -> Maybe Enemy      -- ^ the enemy there if any.
    findEnemy offset =
      lookup ( view playerLocation theMap + offset ) $
        view entities theMap

-- | Draw a row of things
drawRow ::
  forall a e b .
  ( HasIcon e
  , Num a
  , Ord a
  , Num b
  , Ord b
  )
    => (Coord a b -> Maybe e) -- Lookup from coords to posible things
      -> X a                  -- ^ Start position
        -> X a                -- ^ End position
          -> Y b              -- ^ Row relative to the player
            -> Image          -- ^ The image with the things
drawRow lookupThing leftBound rightBound rowOffset
  | leftBound > rightBound
    = backgroundFill 1 1
  | Just thing <- lookupThing (rightBound, rowOffset)
    = go (rightBound - 1) $ char defAttr (view icon thing)
  | otherwise
    = drawRow lookupThing leftBound (rightBound - 1) rowOffset
  where
    go :: X a -> Image -> Image
    go colOffset !img
      -- If the row has negative length the image is empty
      | leftBound > colOffset =
        emptyImage
      | otherwise =
        go (colOffset - 1) $
          case
            lookupThing (colOffset, rowOffset)
          of
            -- If nothing is there put an empty space
            Nothing    ->
              translateX 1 img
            -- If seomthing is there put its icon
            Just thing ->
              char defAttr (view icon thing) <|> img

