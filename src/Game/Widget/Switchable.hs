{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RecursiveDo #-}

module Game.Widget.Switchable where

-- External imports

import Control.Monad.Fix

import Reflex

data Switchable t m a
  = Switchable (m (Event t a))
  | ImmediateSwitch a
  deriving Functor

switchOut ::
  forall t m a b.
    ( MonadFix m
    , MonadHold t m
    , Adjustable t m
    )
      => m (Event t a)
        -> (a -> m b)
          -> m (Event t b)
switchOut initialWidget next = do
  rec
    switchEvent <- headE initialEvents
    widgetResults <- runWithReplace initialWidget $ next <$> switchEvent
    let
      initialEvents :: Event t a
      initialEvents = fst widgetResults

      transformedEvent :: Event t b
      transformedEvent = snd widgetResults
  return transformedEvent

instance (Adjustable t m, MonadFix m, MonadHold t m) => Applicative (Switchable t m) where
  pure = ImmediateSwitch
  (<*>) :: forall a b. Switchable t m (a -> b) -> Switchable t m a -> Switchable t m b
  (ImmediateSwitch f) <*> valueWidget = fmap f valueWidget
  functionWidget <*> (ImmediateSwitch val) = fmap ($val) functionWidget
  (Switchable unswitchableF) <*> (Switchable unswitchableV) =
    Switchable $ switchOut unswitchableF ((<$> unswitchableV) . fmap) >>= switchHold never

instance (Adjustable t m, MonadFix m, MonadHold t m) => Monad (Switchable t m) where
  (>>=) :: forall a b. Switchable t m a -> (a -> Switchable t m b) -> Switchable t m b
  (ImmediateSwitch value) >>= f = f value
  (Switchable unswitchableWidget) >>= f =
    Switchable $ switchOut unswitchableWidget (switchableToEither .  f) >>= flattenEitherEvent
    where
      switchableToEither :: forall t' n x. Monad n => Switchable t' n x -> n (Either x (Event t' x))
      switchableToEither (Switchable unswitchable) = fmap Right unswitchable
      switchableToEither (ImmediateSwitch value) = return $ Left value

      flattenEitherEvent :: forall t' n x. (Reflex t', MonadHold t' n) => Event t' (Either x (Event t' x)) -> n (Event t' x)
      flattenEitherEvent toSwitch = 
        do
          let
            singleEvents :: Event t' x
            singleEvents = fst $ fanEither toSwitch

            eventStreams :: Event t' (Event t' x)
            eventStreams = snd $ fanEither toSwitch
          switchedStreams <- switchHold never eventStreams
          return $ leftmost [singleEvents, switchedStreams]

sinkSwitchable :: (MonadFix m, MonadHold t m, Adjustable t m) => Switchable t m a -> (a -> m (Event t b)) -> m (Event t b)
sinkSwitchable (ImmediateSwitch value) f = f value
sinkSwitchable (Switchable unswitchable) f = switchOut unswitchable f >>= switchHold never

