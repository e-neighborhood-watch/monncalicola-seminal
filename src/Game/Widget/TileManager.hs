{-# LANGUAGE ScopedTypeVariables #-}

module Game.Widget.TileManager
  ( coordSplitH
  ) where

-- Internal imports

import Numeric.Coord
import Numeric.Coord.Vty

-- External imports

import Control.Applicative

import Data.Bitraversable
  ( bisequence
  )

import Reflex.Vty

coordSplitH ::
  forall t m a b.
  ( Reflex t
  , MonadNodeId m
  )
    => Dynamic t (X Int)
      -> Dynamic t (Bool, Bool)
        -> VtyWidget t m a
          -> VtyWidget t m b
            -> VtyWidget t m (a, b)
coordSplitH sideWidth subwidgetFocus centralWidget sideWidget = do
  (screenWidth, screenHeight) <- splitDynPure <$> coordDisplaySize
  let
    centralWidgetRegion :: SafeDynamicRegion t
    centralWidgetRegion = SafeDynamicRegion
      { _safeDynamicRegionTopLeft = bisequence (0, 0)
      , _safeDynamicRegionSize    = bisequence (screenWidth - sideWidth, screenHeight)
      }

    sideWidgetRegion :: SafeDynamicRegion t
    sideWidgetRegion = SafeDynamicRegion
      { _safeDynamicRegionTopLeft = bisequence (screenWidth - sideWidth, 0)
      , _safeDynamicRegionSize    = bisequence (sideWidth, screenHeight)
      }

    (   centralWidgetFocused    :: Dynamic t Bool
      , sideWidgetFocused :: Dynamic t Bool
      )
        = splitDynPure subwidgetFocus
  liftA2 (,)
    ( coordPane centralWidgetRegion    centralWidgetFocused    centralWidget    )
    ( coordPane sideWidgetRegion sideWidgetFocused sideWidget )
