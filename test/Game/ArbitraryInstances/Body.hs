module Game.ArbitraryInstances.Body where

import Game.Type.Body

import Test.QuickCheck

instance Arbitrary BodyPart where
  arbitrary = do
    grasper' <- arbitrary
    vital'   <- arbitrary
    support' <- arbitrary
    intact'  <- arbitrary
    bloodML' <- arbitrary
    return $ BodyPart
      { _grasper = grasper'
      , _vital   = vital'
      , _support = support'
      , _intact  = intact'
      , _bloodML = bloodML'
      }
