{-# Language ScopedTypeVariables #-}
module Game.AttackSpec where

-- Main Library imports
import Game.Attack
import Game.Type.Body

-- Test Library imports
import Game.ArbitraryInstances.Body

-- External Library imports
import Control.Lens
import Test.Hspec
import Test.QuickCheck



spec :: Spec
spec = do
  describe "destroyPart" $ do
    it "Does not modify vital" $ property $
      \ (part :: BodyPart) ->
        view vital (destroyPart part) `shouldBe` view vital part
      
